import 'package:flutter/material.dart';
import 'package:my_notes/main.dart';

import 'SQLDB.dart';

class UpdateNotes extends StatefulWidget {
  final id;
  final titre;
  final description;
  const UpdateNotes({Key? key, this.id, this.titre, this.description}) : super(key: key);

  @override
  State<UpdateNotes> createState() => _UpdateNotesState();
}

class _UpdateNotesState extends State<UpdateNotes> {
  TextEditingController _Titre = TextEditingController();
  TextEditingController _PrendNte = TextEditingController();

  SQLdb sqLdb = SQLdb();
  Future<void> updateNotes() async {
    int rep = await sqLdb.updateData(
      '''
      UPDATE "notes" SET titre="${_Titre.text}", description="${_PrendNte.text}" WHERE id="${widget.id}"
      '''
    );
    print("${rep}");
    if (rep > 0) {
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>HomePage()), (route) => false);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _Titre.text = widget.titre;
    _PrendNte.text = widget.description;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Modifier la note"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>HomePage()), (route) => false); // Retour à la page précédente
          },
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: IconButton(
                onPressed: () async{
                  if (
                  _Titre.text.isEmpty ||
                      _PrendNte.text.isEmpty
                  ) {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text("Erreur"),
                          content: Text("Svp veuillez remplir les champs."),
                          actions: [
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text("OK"),
                            ),
                          ],
                        );
                      },
                    );
                  }else{
                    updateNotes();
                  }
                },
                icon: Icon(
                  Icons.check, color: Colors.orange,
                )),
          )
        ],
      ),
      body: Container(
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: _Titre,
                  decoration: InputDecoration(
                    hintText: "Titre",

                  ),
                ),
              ),
            ),
            SizedBox(height: 20,),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: _PrendNte,
                  decoration: InputDecoration(
                      hintText: "Description de la note"
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
