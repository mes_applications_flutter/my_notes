import 'package:flutter/material.dart';
import 'package:my_notes/SQLDB.dart';
import 'package:my_notes/page_notes.dart';

import 'main.dart';

class FormulaireNotes extends StatefulWidget {
  const FormulaireNotes({super.key});

  @override
  State<FormulaireNotes> createState() => _FormulaireNotesState();
}

class _FormulaireNotesState extends State<FormulaireNotes> {
  TextEditingController _Titre = TextEditingController();
  TextEditingController _PrendNte = TextEditingController();

  SQLdb sqLdb = SQLdb();
  Future<void> saveNotes() async {
    int rep = await sqLdb.insertData(
        '''INSERT INTO 'notes' (titre, description) VALUES ("${_Titre.text}","${_PrendNte.text}")'''
    );
    print("${rep}");
    if (rep > 0) {
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>HomePage()), (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>HomePage()), (route) => false); // Retour à la page précédente
            },
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: IconButton(
                  onPressed: () async{
                    if (
                    _Titre.text.isEmpty ||
                    _PrendNte.text.isEmpty
                    ) {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text("Erreur"),
                            content: Text("Svp veuillez remplir les champs."),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text("OK"),
                              ),
                            ],
                          );
                        },
                      );
                    }else{
                      saveNotes();
                    }
                  },
                  icon: Icon(

                Icons.check, color: Colors.orange,
              )),
            )
          ],
          title: Text("Note",style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),),
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: _Titre,
                    decoration: InputDecoration(
                      hintText: "Titre de la note",

                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: _PrendNte,
                    decoration: InputDecoration(
                      hintText: "Description de la note"
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
