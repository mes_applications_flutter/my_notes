import 'package:flutter/material.dart';
import 'package:my_notes/SQLDB.dart';
import 'package:my_notes/SQLDB2.dart';
import 'package:my_notes/formulaire_taches.dart';
import 'package:my_notes/updateTaches.dart';

class PageListesTaches extends StatefulWidget {
  const PageListesTaches({super.key});

  @override
  State<PageListesTaches> createState() => _PageListesTachesState();
}

class _PageListesTachesState extends State<PageListesTaches> {
  TextEditingController  _searchController = TextEditingController();
  SQLdb2 sqLdb = SQLdb2();
  Future<List<Map>> getAllTaches() async{
    List<Map> ListesTaches = await sqLdb.getData("SELECT * FROM 'taches'");
    return ListesTaches;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
       return false;
      },
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.white,
          onPressed: (){
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context)=>FormulaireTaches())
            );
          },
          child: Icon(Icons.add, color: Colors.orange,),
        ),
        appBar: AppBar(
          backgroundColor: Colors.black87,
          centerTitle: true,
          title: Text(
            "Listes des tâches",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w800),
          ),
        ),
       body:  Container(

      child: Column(
        children: [
          Container(
            margin: EdgeInsets.all(20.0),
            child: TextField(
              controller: _searchController,
              decoration: InputDecoration(

                hintText: 'Recherches...',
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15.0),),

                ),
              ),
              onChanged: (value) {
                _searchController.text = value;
                print(_searchController.text);
                // You can perform search-related actions here
              },
            ),
          ),
          Expanded(
              flex: 1,
              child: Container(
                child: FutureBuilder(
                  future: getAllTaches(),
                  builder: (context, snp){
                    if (snp.hasData) {
                      List<Map> listeAllTaches = snp.data!;
                      return ListView.builder(
                          itemCount: listeAllTaches.length,
                          itemBuilder: (context, index){
                            return InkWell(
                              onTap: (){
                              },
                              splashColor: Colors.orangeAccent,
                              child: Container(

                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Expanded(
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text("${listeAllTaches[index]['titre']}", style: TextStyle(fontWeight: FontWeight.w600),),
                                                        SizedBox(height: 10,),
                                                        Text("${listeAllTaches[index]['description']}", style: TextStyle(fontWeight: FontWeight.w400),),

                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                    child: Row(
                                                      mainAxisSize: MainAxisSize.min,
                                                      children: [
                                                        TextButton(
                                                            onPressed: (){
                                                              Navigator.of(context).push(
                                                                  MaterialPageRoute(builder: (context)=>UpdateTaches(id:listeAllTaches[index]['id'],titre: listeAllTaches[index]['titre'], description: listeAllTaches[index]['description'],))
                                                              );
                                                            },
                                                            child: Icon(Icons.edit, color: Colors.green,)),
                                                        TextButton(
                                                            onPressed: (){
                                                              showDialog(
                                                                  context: context,
                                                                  builder: (context)=>AlertDialog(
                                                                    title: Text("Voulez-vous vraiment supprimer ${listeAllTaches[index]['titre']} ?"),
                                                                    actions: [
                                                                      ElevatedButton(
                                                                          onPressed: ()async{
                                                                            int rep = await sqLdb.deleteData(
                                                                                '''
                                                                                      DELETE FROM "taches" WHERE id = ${listeAllTaches[index]['id']}
                                                                                      '''
                                                                            );
                                                                            if (rep>0) {
                                                                              Navigator.of(context).pop();
                                                                              setState(() {

                                                                              });
                                                                            }
                                                                          },
                                                                          child: Text("Oui", style: TextStyle(color: Colors.orange),)),
                                                                      ElevatedButton(
                                                                          onPressed: (){
                                                                            Navigator.of(context).pop();
                                                                          },
                                                                          child: Text("Non", style: TextStyle(color: Colors.green),))
                                                                    ],
                                                                  )
                                                              );
                                                            },
                                                            child: Icon(Icons.delete, color: Colors.orange,)),
                                                      ],
                                                    )
                                                )
                                              ],
                                            ),
                                            SizedBox(height: 10,),
                                            Container(
                                              color: Colors.grey[400],
                                              height: 0.5,
                                            )
                                          ],
                                        )
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }
                      ) ;
                    }  else{
                      return Center(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Colors.orange),
                        ),
                      );
                    }
                  },
                ),
              )
          )
        ],
      ),
    ),

      ),
    );
  }
}
