import 'package:flutter/material.dart';
import 'package:my_notes/SQLDB2.dart';
import 'package:my_notes/main.dart';
import 'package:my_notes/page_listes-taches.dart';
import 'package:my_notes/page_notes.dart';
import 'SQLDB.dart';

class FormulaireTaches extends StatefulWidget {
  const FormulaireTaches({super.key});

  @override
  State<FormulaireTaches> createState() => _FormulaireTachesState();
}

class _FormulaireTachesState extends State<FormulaireTaches> {
  TextEditingController _Titre = TextEditingController();
  TextEditingController _PrendNte = TextEditingController();

  SQLdb2 sqLdb = SQLdb2();
  Future<void> saveNotes() async {
    int rep = await sqLdb.insertData(
        '''INSERT INTO 'taches' (titre, description) VALUES ("${_Titre.text}","${_PrendNte.text}")'''
    );
    print("${rep}");
    if (rep > 0) {
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>HomePage()), (route) => false);
    }
  }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context)=>HomePage()), (route) => false); // Retour à la page précédente
            },
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: IconButton(
                  onPressed: () async{
                    if (
                    _Titre.text.isEmpty ||
                        _PrendNte.text.isEmpty
                    ) {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text("Erreur"),
                            content: Text("Svp veuillez remplir les champs."),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text("OK"),
                              ),
                            ],
                          );
                        },
                      );
                    }else{
                      saveNotes();
                    }
                  },
                  icon: Icon(

                    Icons.check, color: Colors.orange,
                  )),
            )
          ],
          title: Text("Tâche",style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),),
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: _Titre,
                    decoration: InputDecoration(
                      hintText: "Titre de la tâches",

                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: _PrendNte,
                    decoration: InputDecoration(
                        hintText: "Description de la tâche"
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
