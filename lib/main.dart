import 'dart:async';

import 'package:flutter/material.dart';
import 'package:my_notes/formulaire_notes.dart';
import 'package:my_notes/formulaire_taches.dart';
import 'package:my_notes/page_listes-taches.dart';
import 'package:my_notes/page_notes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
     debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: Center(
        child: Chargement(),
      ),
      routes: {
       "formulaire_taches":(context)=>FormulaireTaches(),
        "formulaire_notes":(context)=>FormulaireNotes(),
        "page_note":(context)=>PageNotes()
      },
    );
  }
}


class Chargement extends StatefulWidget {
  const Chargement({super.key});

  @override
  State<Chargement> createState() => _ChargementState();
}

class _ChargementState extends State<Chargement> {
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds:8), () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=> HomePage()));
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset("assets/icon/icon.png", height: 100,),
            SizedBox(height:50),
            CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.orange),
            )
          ],
        ),
      ),
    );
  }
}


class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  int _selectedIndex = 0;

  static List<Widget> _widgetOptions = <Widget>[
    PageNotes(),
    PageListesTaches(),
  ];

  static List<String> _appBarTitles = <String>[
    'Notes',
    'Liste des tâches'
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.black87,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.note_add),
            label: 'Notes',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: 'Tâches',
          ),

        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.orange,
        unselectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }
}

















