import 'package:flutter/material.dart';
import 'package:my_notes/formulaire_notes.dart';
import 'package:my_notes/updateNotes.dart';

import 'SQLDB.dart';

class PageNotes extends StatefulWidget {
  const PageNotes({super.key});

  @override
  State<PageNotes> createState() => _PageNotesState();
}

class _PageNotesState extends State<PageNotes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SearchBarDemo(),
    );
  }
}


class SearchBarDemo extends StatefulWidget {
  @override
  _SearchBarDemoState createState() => _SearchBarDemoState();
}

class _SearchBarDemoState extends State<SearchBarDemo> {
  TextEditingController _searchController = TextEditingController();
  SQLdb sqLdb = SQLdb();
  Future<List<Map>> getAllNotes() async{
    List<Map> ListesNotes = await sqLdb.getData("SELECT * FROM 'notes'");
    return ListesNotes;
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.white,
            onPressed: (){
              Navigator.of(context).pushNamed("formulaire_notes");
            },
            child: Icon(Icons.add, color: Colors.orange,),
          ),
          appBar: AppBar(
            backgroundColor: Colors.black87,
            centerTitle: true,
            title: Text('Listes des note',
              style: TextStyle(color: Colors.white,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          body:  Container(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.all(20.0),
                  child: TextField(
                    controller: _searchController,
                    decoration: InputDecoration(
                      hintText: 'Recherches...',
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15.0),),
                      ),
                    ),
                    onChanged: (value) {
                      _searchController.text = value;
                      print(_searchController.text);
                      // You can perform search-related actions here
                    },
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: Container(
                      child: FutureBuilder(
                        future: getAllNotes(),
                        builder: (context, snp){
                          if (snp.hasData) {
                            List<Map> listeAllNotes = snp.data!;
                            return ListView.builder(
                                itemCount: listeAllNotes.length,
                                itemBuilder: (context, index){
                                  return InkWell(
                                    onTap: (){
                                    },
                                    splashColor: Colors.orangeAccent,
                                    child: Container(
                                      child: Row(
                                        children: [
                                          Expanded(
                                              child: Column(
                                                children: [
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Expanded(
                                                        child: Padding(
                                                          padding: const EdgeInsets.all(8.0),
                                                          child: Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: [
                                                              Text("${listeAllNotes[index]['titre']}", style: TextStyle(fontWeight: FontWeight.w600),),
                                                              SizedBox(height: 10,),
                                                              Text("${listeAllNotes[index]['description']}", style: TextStyle(fontWeight: FontWeight.w400),),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                          child: Row(
                                                            mainAxisSize: MainAxisSize.min,
                                                            children: [
                                                              TextButton(
                                                                  onPressed: (){
                                                                    Navigator.of(context).push(
                                                                      MaterialPageRoute(builder: (context)=>UpdateNotes(id:listeAllNotes[index]['id'],titre: listeAllNotes[index]['titre'], description: listeAllNotes[index]['description'],))
                                                                    );
                                                                  },
                                                                  child: Icon(Icons.edit, color: Colors.green,)),
                                                              TextButton(
                                                                  onPressed: (){
                                                                      showDialog(
                                                                          context: context, 
                                                                          builder: (context)=>AlertDialog(
                                                                            title: Text("Voulez-vous vraiment supprimer ${listeAllNotes[index]['titre']} ?"),
                                                                            actions: [
                                                                              ElevatedButton(
                                                                                  onPressed: ()async{
                                                                                    int rep = await sqLdb.deleteData(
                                                                                      '''
                                                                                      DELETE FROM "notes" WHERE id = ${listeAllNotes[index]['id']}
                                                                                      '''
                                                                                    );
                                                                                    if (rep>0) {
                                                                                      Navigator.of(context).pop();
                                                                                      setState(() {

                                                                                      });
                                                                                    }  
                                                                                  }, 
                                                                                  child: Text("Oui", style: TextStyle(color: Colors.orange),)),
                                                                              ElevatedButton(
                                                                                  onPressed: (){
                                                                                    Navigator.of(context).pop();
                                                                                  },
                                                                                  child: Text("Non", style: TextStyle(color: Colors.green),))
                                                                            ],
                                                                          )
                                                                      );
                                                                  },
                                                                  child: Icon(Icons.delete, color: Colors.orange,)),
                                                            ],
                                                          )
                                                      )
                                                    ],
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Container(
                                                    color: Colors.grey[400],
                                                    height: 0.5,
                                                  )
                                                ],
                                              )
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                            ) ;
                          }  else{
                            return Center(
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation(Colors.orange),
                              ),
                            );
                          }
                        },
                      ),
                    )
                ),

              ],
            ),
          ),
        ),
        onWillPop: () async{
          return false;
        }
    );
  }
}
